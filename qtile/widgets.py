from libqtile import layout, bar, widget
from libqtile.lazy import lazy
from libqtile.config import Screen
from fontawesome import icons
import os, subprocess, iwlib, inspect
from groups import visible_workspaces
#from mpd import MPDClient

from function import colors
from settings import decor, colored, margin, reverse, font, mywidgets, opacity, size

import mods.widgets as mods

class HColor: # Human-readable color names
    bar_background = colors[0]
    bar_font       = colors[7]
    bar_color      = colors[2]
    bar_color_light= colors[3]
    bar_color_font = colors[0]
class ColorScheme:
    def __init__(self):
        self.light   = self.lightScheme
        self.active  = self.activeScheme
        self.inactive= self.inactiveScheme
    class Scheme:
        def __init__(self, to=None):
            self.item = to
            self.foreground = None
            self.background = None
        def palette(self, full, simple):
            if colored == 'full' or colored == 'compact' and self.item in reverse:
                return full
            else:
                return simple
    def lightScheme(self,to=None):
        light = self.Scheme(to)
        light.foreground = light.palette(colors[0],colors[7])
        light.background = light.palette(colors[7],colors[0])
        return light
    def activeScheme(self,to=None):
        active = self.Scheme(to)
        active.foreground = active.palette(colors[0],colors[7])
        active.background = active.palette(colors[2],colors[0])
        return active
    def inactiveScheme(self):
        inactive = self.Scheme()
        inactive.foreground = colors[8]
        inactive.background = colors[1]
        return inactive

color = ColorScheme()

widget_defaults = dict(
    font = font,
    fontsize = 14,
    padding = 0
)

thermic = widget.ThermalSensor(
    padding = 2,
    fontsize = 12,
    threshold = 75,
    update_interval = 15,
)

memory = widget.Memory(
    padding = 2,
    fontsize = 14,
    format = '{MemUsed: .0f}MB ',
)

decor = widget.TextBox(
    text = decor,
    width = 5,
    fonts="droid sans mono for powerline",
    foreground = color.light('decor').foreground,
    background = color.light('decor').background,
    font_size=15,
)

sep=widget.Sep(
	size_percent=100,
	padding=2,
	linewidth=1,
	foreground='ffffff'
)

prompt = widget.Prompt(background=colors[5])
spacer = widget.Spacer()
systray= widget.Systray()
room = mods.RoomBox(
	padding=5,
	fontsize=12,
)

battery = widget.Battery(
        low_foreground = colors[0] if colored == 'full' or colored == 'compact' and 'battery' in reverse else colors[7],
        format = '{char} {percent:2.0%}',
        foreground = colors[0] if colored == 'full' or colored == 'compact' and 'battery' in reverse else colors[7],
        background = colors[7] if colored == 'full' or colored == 'compact' and 'battery' in reverse else colors[0],
	discharge_char=icons['caret-down'],
	charge_char=icons['caret-up'],
	full_char=icons['smile'],
	update_interval=1,
        padding = 5
        )
groupbox = widget.GroupBox(
        borderwidth = 1,
        disable_drag = True,
        #highlight_method = 'block',
        inactive = HColor.bar_font,
        #active = HColor.bar_font,
        active = HColor.bar_color_light,
        highlight_color = HColor.bar_color_font,
        highlight_method = 'block',
        #this_current_screen_border = HColor.bar_font,
        this_current_screen_border = HColor.bar_color,
        # this_screen_border = colors [4],
        # other_current_screen_border = colors[6],
        # other_screen_border = colors[4],
        padding = 8,
        #margin_x =20,
        #block_highlight_text_color = HColor.bar_color,
        block_highlight_text_color = HColor.bar_background,
        visible_groups = visible_workspaces(),
        #spacing = 5,
        #foreground = color.light().foreground,
        #background = color.light('groupbox').foreground,
        )

windowname = mods.WindowName(
        max_chars = 50,
        foreground = color.active('windowname').foreground,
        background = color.active('windowname').background,
	empty_color = HColor.bar_background,
	parse_text = mods.WindowName.ridback,
	padding=5,
        )

volume_icon = widget.TextBox(
       text = '墳 ',
        foreground = color.light('volume_icon').foreground,
        background = color.light('vlume_icon').background,
	fontsize=12,
	padding=2
        )

volume = widget.Volume(
        update_interval = 1,
        background = color.light('volume').background,
        foreground = color.light('volume').foreground,
	padding=2
        )


clock = widget.Clock(
        format = '%I:%M%p',
        foreground = color.light('clock').foreground,
        background = color.light('clock').background,
	fontsize=14,
	padding=2
        )

date= widget.Clock(
	format = '%a %d %b',
	foreground = color.light('date').foreground,
	background = color.light('date').background,
	fontsize=12,
	padding=2
)

#currentlayout = widget.CurrentLayout(
#        padding = 20,
#        foreground = color.light('currentlayout').foreground,
#        background = color.light('currentlayout').background,
#        )

currentlayout = widget.CurrentLayoutIcon(padding=5)

# Screen, topbar
# Load widgets from settings
widgets = []
for wid in mywidgets:
    widgets.append(eval(wid))



screens = [
    Screen(
        top = bar.Bar(
            widgets=widgets,
            size = size,
            background = colors[0],
            opacity = opacity,
            #margin = [margin,margin,0,margin]
        ),
    )
]
