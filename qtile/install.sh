sudo pacman -Syyu
sudo pacman -S python3 python-pip picom imagemagick procps feh nitrogen mpd otf-fantasque-sans-mono kitty alacratty trayer ttf-mononoki
yay -S ttf-consolas-ligaturized betterlockscreen
pip3 install --user pywal
export PATH="${PATH}:${HOME}/.local/bin/"
echo "Needed to add local-bin path to init shell"

#Generate cache, and then read it with qtile on reboot
nitrogen --random /usr/share/gruvbox --auto --save
wal -i 
