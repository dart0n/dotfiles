#!/usr/bin/env bash


## Autostart Programs

# Kill already running process
_ps=(picom dunst ksuperkey xfce-polkit xfce4-power-manager)
for _prs in "${_ps[@]}"; do
	if [[ `pidof ${_prs}` ]]; then
		killall -9 ${_prs}
	fi
done

# Init files
#~/.xinitrc &
#feh --no-fehbg --bg-fill '/home/d0/Pictures/wallpapers/wallpapers/room.jpg'
#supervisor restart & #Run xmobar 

# polkit agent
#/usr/lib/xfce-polkit/xfce-polkit &

# Enable power management
#xfce4-power-manager &

# Enable Super Keys For Menu
#ksuperkey -e 'Super_L=Alt_L|F1' &
#ksuperkey -e 'Super_R=Alt_L|F1' &

# Restore wallpaper
# WALCOLORS
#wal -i $(cat )

# Lauch notification daemon
#~/.config/i3/bin/i3dunst.sh
if [[ `pidof dunst` ]]; then
	pkill dunst
fi

# dunst \
# -geom "280x50-10+40" -frame_width "1" -font "Iosevka 10" \
# -lb "#090A0B" -lf "#E6DFE0" -lfr "#E6DFE0" \
# -nb "#090A0B" -nf "#E6DFE0" -nfr "#E6DFE0" \
# -cb "#090A0B" -cf "#BB553F" -cfr "#BB553F" &

# Lauch polybar
# ~/.config/i3/bin/i3bar.sh
# polybar main -c .xmonad/polybar/config.ini &
#setsid -f xmobar  &


# Lauch compositor
# ~/.config/i3/bin/i3comp.sh
exec picom &

# Adjust backlight (AMD)
#blight -d amdgpu_bl0 set 15%


#startup &
