from libqtile.command import lazy
from libqtile.config import Key, KeyChord, EzKey
from settings import default_apps
from function import Functions
from wallpaper import *
from groups import groups,to_room,window_to_room,to_space,window_to_space,workspace,goup,godown,windown,winup
from os import path

def new_wallpaper():
    def f(qtile):
        qtile.cmd_spawn(path.expanduser('~/.config/qtile/wallpaper.py')+' new')
        qtile.mouse_map.clear()
        qtile.groups_map.clear()
        qtile.groups.clear()
        qtile.screens.clear()
        qtile.load_config()
        qtile.cmd_restart()
    return f

# Default mod keys
mod0 = ['mod4']
mod1 = ['mod1']
mod2 = ['mod4', 'shift']
mod3 = ['mod4', 'control']

# Default applications
mybrowser = default_apps['browser']
myterminal = default_apps['terminal']
myeditor = default_apps['editor']
myfm = default_apps['file']

# Keys
keys = [

    # Keychord
    KeyChord(mod0, 't', [
        Key([], 'h', lazy.layout.swap_left()),
        Key([], 'l', lazy.layout.swap_right()),
        Key([], 'j', lazy.layout.shuffle_down()),
        Key([], 'k', lazy.layout.shuffle_up()),
    ],
             mode = 'move'
             ),
#    KeyChord(mod0, 'r', [
#        Key([], 'w', lazy.spawn(mybrowser)),
#        Key([], 'e', lazy.spawn(myeditor)),
#        Key([], 'f', lazy.spawn(myfm)),
#        Key([], 'n', lazy.spawn('nitrogen')),
#    ],
#             mode = 'run'
#             ),
    #KeyChord(mod0, 'o', [
    #    Key([], 'q', lazy.spawn('kitty .config/qtile')),
    #    Key([], 'c', lazy.spawn('kitty .config'))
    #],
    #         mode = 'open'
    #         ),

    Key(mod3, 'w', lazy.spawn(mybrowser)),
    Key(mod0, 'n', lazy.function(new_wallpaper())),

    # Layout hotkeys
    Key(mod0, 'h', lazy.layout.shrink_main()),
    Key(mod0, 'l', lazy.layout.grow_main()),
    Key(mod0, 'j', lazy.layout.up()),
    Key(mod0, 'k', lazy.layout.down()),
    Key(mod2, 'h', lazy.layout.swap_left()),
    Key(mod2, 'l', lazy.layout.swap_right()),
    Key(mod2, 'j', lazy.layout.shuffle_down()),
    Key(mod2, 'k', lazy.layout.shuffle_up()),
    Key(mod0, 'f', lazy.layout.maximize()),
    Key(mod2, 'n', lazy.layout.normalize()),
    Key(mod2, 'space', lazy.layout.flip()),

    # Window hotkeys
    Key(mod0, 'space', lazy.next_layout()),
    #Key(mod0, 'q', lazy.window.kill()),
    Key(mod0, 'w', lazy.window.kill()),
    Key(mod2, 'q', Functions.kill_all_windows()),
    Key(mod2, 'o', Functions.kill_all_windows_minus_current()),
    # Key(mod2, 'c', lazy.window.kill()),
    Key(mod2, 'space', lazy.window.toggle_floating()),

    # Spec hotkeys
    Key(mod0, 'Return', lazy.spawn(myterminal)),
    Key(mod3, 'r', lazy.restart()),
    Key(mod3, 'q', lazy.shutdown()),

    # Apps hotkeys
    Key(mod0, 'r', lazy.spawncmd()),
    Key(mod2, 'f', lazy.spawn(myfm)),
    Key(mod2, 'w', lazy.spawn(mybrowser)),
    Key(mod3, 'c', lazy.spawn('kitty --working-directory /home/d0/.config/qtile')),


    # System hotkeys
    # Key([mod0, 'shift', 'control'], 'F11', lazy.spawn('sudo hibernate-reboot')),
    # Key([mod0, 'shift', 'control'], 'F12', lazy.spawn('systemctl hibernate')),
    Key([], 'Print', lazy.spawn("scrot -e 'mv $f /home/user/screenshots/'")),

    # Media hotkeys
    Key([], 'XF86AudioRaiseVolume', lazy.spawn(path.expanduser('~/.local/bin/volume inc'))),
    Key([], 'XF86AudioLowerVolume', lazy.spawn(path.expanduser('~/.local/bin/volume dec'))),
    Key([], 'XF86AudioMute', lazy.spawn(path.expanduser('~/.local/bin/volume mute'))),
    Key([], 'XF86MonBrightnessUp', lazy.spawn(path.expanduser('~/.local/bin/brightness inc'))),
    Key([], 'XF86MonBrightnessDown', lazy.spawn(path.expanduser('~/.local/bin/brightness dec'))),
    Key([], 'XF86WebCam', lazy.spawn(path.expanduser('~/.local/bin/brightness toggle'))),

    #Key(mod0, 'p', lazy.spawn('xpenguins --nomenu')),

    # Rofi
    #Key(mod0, 'p', lazy.spawn('.config/qtile/rofi/bin/launcher')),
    #Key(mod0, 'm',lazy.spawn('.config/qtile/rofi/bin/mpd')),
    #Key(mod0, 'n',lazy.spawn('.config/qtile/rofi/bin/network_menu')),
    #Key(mod0, 's',lazy.spawn('.config/qtile/rofi/bin/screenshot')),
    #Key(mod0, 'w',lazy.spawn('.config/qtile/rofi/bin/windows')),
    #Key(mod0, 'x',lazy.spawn('.config/qtile/rofi/bin/powermenu')),

]


groupkeys=[]
for space in [1,2,3,4]:
	#groupkeys.extend([
	#	Key(mod0, str(space), lazy.group[str(space)].toscreen()),
	#	Key(mod2, str(space), lazy.window.togroup(str(space))),
	#])
	groupkeys.extend([
		Key(mod0, str(space), lazy.function(to_space(str(space)))),
		Key(mod2, str(space), lazy.function(window_to_space(str(space)))),

	])
	for _ix, room in enumerate(groups[:space]):
		_ix+=1
		groupkeys.extend([
			Key(['mod4'], 'Down',		lazy.function(godown())),
			Key(['mod4','shift'], 'Down',	lazy.function(windown())),
			Key(['mod4'], 'Up',  		lazy.function(goup())),
			Key(['mod4','shift'], 'Up',  	lazy.function(winup())),
			Key(['mod4','shift'], '0', lazy.function(to_room('1'))),
		])
		#groupkeys.extend([
		#	Key(mod0,str(_ix),lazy.group[group.name].toscreen()),
		#	Key(mod2,str(_ix),lazy.window.togroup(group.name)),
		#])

keys.extend(groupkeys)
