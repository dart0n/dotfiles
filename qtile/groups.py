from libqtile.config import Group, ScratchPad, DropDown, Match
from fontawesome import icons
groups =[]
# With Icons
spaces = [
		Group(name='1',label="", layout='monadwide'),
		Group(name='2',label="", layout='max', matches=[Match(wm_class='Firefox')]),
		Group(name='3',label="", layout='floating'),
		Group(name='4',label="", layout='monadtall')
]

# With numbers
#spaces = [
#		Group(name='1',label='1', layout='monadtall'),
#		Group(name='2',label='2', layout='max', matches=[Match(wm_class='Firefox')]),
#		Group(name='3',label='3', layout='floating'),
#		Group(name='4',label='4', layout='monadtall')
#]

def group_name(space,room):
	return '%s%s' % (space,room)

global workspace, rooms
rooms = '12345'
workspace = {
	'current': spaces[0].name,
	'jobs': {},
}

for space in spaces:
	workspace['jobs'][space.name]={
		'work_in':rooms[0]
	}

def current_room(workspace):
	return workspace['jobs'][workspace['current']]['work_in']

def visible_workspaces():
	global workspace
	visible=[]
	for space in workspace['jobs']:
		if space == workspace['current']:
			visible.append(group_name(space,workspace['jobs'][space]['work_in']))
		else:
			visible.append(group_name(space,'1'))
	return visible

def update_groupbox():
	def f(qtile):
		qtile.widgets_map['groupbox'].visible_groups=visible_workspaces()
		qtile.widgets_map['groupbox'].draw()
	return f
def update_roombox():
	def f(qtile):
		#print(qtile.widgets_map['textbox'][0])
		qtile.widgets_map['roombox'].text = '['+str(workspace['jobs'][workspace['current']]['work_in'])+']'
		qtile.widgets_map['roombox'].draw()
	return f
def room_names(workspace):
	return [group_name(workspace,room) for room in rooms]

def to_space(space):
	def f(qtile):
		global workspace
		workspace['current']=space
		if space in workspace['jobs']:
			group =group_name(space,workspace['jobs'][space]['work_in'])
			if len(qtile.groups_map[group].windows) == 0:
				lwin,room=0,int(workspace['jobs'][space]['work_in'])
				while (lwin == 0) and (room > 1):
					room -= 1
					qtile.cmd_function(goup())
					ngroup=group_name(workspace['current'],str(room))
					lwin = len(qtile.groups_map[ngroup].windows)
				qtile.groups_map[group_name(workspace['current'],str(room))].cmd_toscreen(toggle = False)
			else:
				qtile.groups_map[group].cmd_toscreen(toggle = False)
		else:
			group =group_name(space,'1')
			qtile.groups_map[group].cmd_toscreen(toggle = False)
		qtile.cmd_function(update_groupbox())
	return f

def window_to_space(space, room = '1'):
	def f(qtile):
		global workspace
		workspace['current']=space
		qtile.current_window.togroup(group_name(space,room))
	return f

def to_room(room):
	def f(qtile):
		global workspace
		qtile.groups_map[group_name(workspace['current'],room)].cmd_toscreen(toggle = False)
	return f

def window_to_room(room):
	def f(qtile):
		global workspace
		workspace['jobs'][workspace['current']]={'work_in':room}
		qtile.current_window.togroup(group_name(workspace['current'],room))
	return f

def goup():
	def f(qtile):
		global workspace
		current_room = int(workspace['jobs'][workspace['current']]['work_in'])
		if current_room > 1:
			new_room = current_room-1
			new_group= group_name(workspace['current'],str(new_room))
			qtile.groups_map[new_group].cmd_toscreen(toggle = False)
			workspace['jobs'][workspace['current']]['work_in'] = new_room
			#qtile.cmd_function(update_roombox())
			#qtile.cmd_function(update_groupbox())
	return f
def godown():
	def f(qtile):
		global workspace
		current_room = int(workspace['jobs'][workspace['current']]['work_in'])
		if current_room < 5:
			new_room = current_room+1
        #qtile.cmd_function(update_roombox())
        #qtile.cmd_function(update_groupbox())
		else:
			new_room = 1

		new_group= group_name(workspace['current'],str(new_room))
		qtile.groups_map[new_group].cmd_toscreen(toggle = False)
		workspace['jobs'][workspace['current']]['work_in'] = new_room
	return f

def winup():
	def f(qtile):
		global workspace
		current_room = int(workspace['jobs'][workspace['current']]['work_in'])
		if current_room > 1:
			new_room  = current_room-1
			new_space = workspace['current']
			new_group = group_name(new_space,str(new_room))
			qtile.current_window.togroup(new_group)
	return f
def windown():
	def f(qtile):
		global workspace
		current_room = int(workspace['jobs'][workspace['current']]['work_in'])
		if current_room < 5:
			new_room  = current_room+1
			new_space = workspace['current']
			new_group = group_name(new_space,str(new_room))
			qtile.current_window.togroup(new_group)
	return f

for space in spaces:
	for room in rooms:
		groups.append(Group(group_name(space.name,room),label=space.label,layout=space.layout))

#print(groups)
#print(workspace)
