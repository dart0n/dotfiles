#!/usr/bin/python3
import os
import sys

def cached_background():
        with open(os.path.expanduser('~/.config/nitrogen/bg-saved.cfg')) as saved:
                file=saved.readlines()[1].replace('\n','').split('=')[1]
        return file

def cache_colors():
        os.system('wal -i '+cached_background())
        return 0

def new_background():
	os.system('nitrogen --random --set-auto --save')
	cache_colors()
	return 0

def set_cached():
	os.system('nitrogen --restore')
	cache_colors()
	return 0

def load_cached_colors(cache=os.path.expanduser('~/.cache/wal/colors')):
	colors=[]
	with open(cache) as fcolors:
		for i in range(16):
			colors.append(fcolors.readline().strip())
	colors.append('#ffffff')
	return colors

if len(sys.argv)==2:
	if 'new' in sys.argv:
		new_background()
		
	elif 'old' in sys.argv:
		set_cached()
