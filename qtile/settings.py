# Global margin for windows and the bar
margin = 3
#margin = 8

## BAR SETTINGS ##

size = 18
opacity = 1.0
# Determine the bar color style. Choose from 'none', 'full', 'compact'
colored = 'compact'
#colored = 'none'
# Arrange the widget order
mywidgets = [
	'currentlayout','sep',
	'groupbox',
	'room',
	'windowname','prompt','sep',
#    'volume','volume_icon','sep',
    'thermic','sep',
    'memory','sep',
	'date','sep',
	'clock','sep',
	'battery'
]

# Widgets to be showed in a reversed bg and fg in compact mode
reverse = ['currentlayout', 'chord', 'battery','windowname']
# The seprator on the bar
#decor = "\ue0be" #POWERLINE_SYMBOL
# decor = '  '
decor = '┃'
font = 'Iosevka Nerd Font Regular'
#font = 'font awesome'
#font = 'FantasqueSansMono Nerd Font Regular'
# font = 'DMMono Nerd Font'
# font = 'Noto Sans Sc Bold'


default_apps = {
  #'browser' : 'icecat',
  'browser': 'webbrowser',
  'terminal' : 'kitty',
  #'terminal' : 'alacritty',
  'editor' : 'kitty nano',
  'file' : 'Thunar',
}

# List of available workspaces.
# Each workspace has its own prefix and hotkey.
workspaces = [
    #name hotkey
    #(' ', 'a'),
    #(' ', 'b'),
    #('', 'c'),
    #(' ', 'd'),
    #(' ', 'e'),
    ('a', 'a'),
    ('b', 'b'),
    ('c', 'c'),
    ('d', 'd'),
    ('e', 'e'),
]

# List of available rooms.
# Rooms are identical between workspaces, but they can
# be changed to different ones as well. Minor changes required.
rooms = '123456'
