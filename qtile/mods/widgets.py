from libqtile import bar, hook, pangocffi
from libqtile.widget import base
from libqtile.log_utils import logger

class RoomBox(base.InLoopPollText):
	# Can't update 'workspace-room' variable locally, doing globally
	defaults = [
		('format', '[{room}]', 'How to format the text'),
		('update_interval', 0.7, 'Update interval to draw the actual room space'),
	]
	def __init__(self, **config):
		super().__init__("", **config)
		base.InLoopPollText.__init__(self, **config)
		self.add_defaults(RoomBox.defaults)
		#base._TextBox.__init__(self, '', width, **config)
	#def refresh(self):
	#	self.update(self.poll())
	#	return self.update_interval - time() % self.update_interval
	def poll(self):
		online = self.bar.screen.group
		if online.windows:
			current_room = online.name[1]
			return self.format.format(room=current_room)
		else:
			return self.format.format(room='')

class WindowName(base._TextBox):
    """Displays the name of the window that currently has focus"""
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('for_current_screen', False, 'instead of this bars screen use currently active screen'),
        ('empty_group_string', ' ', 'string to display when no windows are focused on current group'),
        ('empty_color', '#000000', 'color to fill when there are no windows (focused) on the current group'),
        ('format', '{state}{name}', 'format of the text'),
        ('parse_text', None, 'Function to parse and modify window names. '
         'e.g. function in config that removes excess '
         'strings from window name: '
         'def my_func(text)'
         '    for string in [\" - Chromium\", \" - Firefox\"]:'
         '        text = text.replace(string, \"\")'
         '   return text'
         'then set option parse_text=my_func'),
    ]

    def __init__(self, width=bar.STRETCH, **config):
        self.init_textbox(width=width, **config)
        self.add_defaults(WindowName.defaults)
        if self.background: self.bc = self.background

    def init_textbox(self,width, **config):
        base._TextBox.__init__(self, width=width, **config)

    def config_textbox(self, qtile, bar):
        base._TextBox._configure(self, qtile, bar)

    def _configure(self, qtile, bar):
        self.config_textbox(qtile,bar)
        hook.subscribe.client_name_updated(self.hook_response)
        hook.subscribe.focus_change(self.hook_response)
        hook.subscribe.float_change(self.hook_response)
        @hook.subscribe.current_screen_change
        def on_screen_changed():
            if self.for_current_screen:
                self.hook_response()

    def ridback(text):
        # Rid backslash'es
        for pattern in ['- Chromium', '- Firefox']:
                text = text.replace(pattern, '')
        return text

    def hook_response(self, *args):
        if self.for_current_screen:
            w = self.qtile.current_screen.group.current_window
        else:
            w = self.bar.screen.group.current_window
        state = ''
        if w:
            if w.maximized:
                state = '[] '
            elif w.minimized:
                state = '_ '
            elif w.floating:
                state = 'V '
            var = {}
            var["state"] = state
            var["name"] = w.name
            if callable(self.parse_text):
                try:
                    var["name"] = self.parse_text(var["name"])
                except:
                    logger.exception("parse_text function failed:")
            wm_class = w.get_wm_class()
            var["class"] = wm_class[0] if wm_class else ""
            unescaped = '{state}{name}'.format(**var)
            self.online = True
            # yep windows
        else:
            unescaped = self.empty_group_string
            self.online = False
            # no windows
        ffiStr = unescaped
        if not self.online:
            self.background = self.empty_color
        else:
            self.background = self.bc            

        text = pangocffi.markup_escape_text(ffiStr)
        self.update(text)
