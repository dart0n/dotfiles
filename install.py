#!/usr/bin/python3

import os
global gvars
gvars = {}

class w:
    def __init__(self, skeleton = 'install.sh'):
        self.skeleton = open(skeleton).readlines()
        self.corp     = {}
        self.bone     = None
        self.ptr      = 0
    def _a_bone(self,bone):
        if '#' in bone and bone[0] == '#': pass
        else: return False
        return True
    
    def parse_bones(self):
        while self.ptr < len(self.skeleton):
            bone = self.skeleton[self.ptr]
            if self._a_bone(bone):
                self.bone= bone.split('# ')[1].replace('\n','')
            else:
                self.corp[self.bone]=[]
                self.corp[self.bone].extend(self.parse_corp())
            self.ptr+=1

    def parse_corp(self):
        nn   = self.ptr
        corps=[]
        while nn < len(self.skeleton):
            skin= self.skeleton[nn]
            skin= skin.replace('\n','') if skin != '\n' else ''
            if not self._a_bone(skin):
                if skin != '':
                    corps.append(skin)
                #print(nn,skin)
            else:
                nn-=1
                break
            nn+=1
        self.ptr=nn
        return corps

def drop_bones_v(bone):
    for val in bone:
        print('    |_',val)

def drop_skeleton(bones,askfor = True):
    for key in bones.corp:
        print(key)
        drop_bones_v(bones.corp[key])
        wantto = input('    |-Do u want to exec this ? ') if askfor else False
        for val in bones.corp[key]:
            if wantto in ("y","Y","s","S","yes","Yes"):
                if '$' in val:
                    val=replace_var(val)
                if '=' in val:
                    setting_var(val)
                elif '=' not in val:
                    os.system('bash -c \''+val+'\'')
    return bones.corp

def replace_var(pattern):
    global gvars
    # print(gvars)
    var = pattern[pattern.index('$')+1]
    # print(var)
    if var in gvars:
        new_pattern=pattern.replace('$'+var,gvars[var])
        if '$' in new_pattern:
            return replace_var(new_pattern)
        else:
            return new_pattern
    else:
        if var == '(':
            return pattern
        else:
            return None
def setting_var(pattern):
    global gvars
    assert(pattern.count('=') == 1,'The pattern isnt a declaration')
    assert(pattern.split('=')[0].count(' ') >= 1 and pattern.split('=')[1].count(' ') >= 1,'Dont have spaces between var and val')
    if len(pattern.split('=')) == 2:
        var,val = pattern.split('=')[0],pattern.split('=')[1]
        var = var.replace(' ','') if ' ' in var else var
        val = val.replace(' ','') if ' ' in val else val
        val = val.replace('"','') if '"' in val else val
        if '$' in val:
            val=replace_var(val)
        gvars[var] = val
    # print(gvars)

bones=w()
bones.parse_bones()
drop_skeleton(bones)
