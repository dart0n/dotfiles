<a href="https://gitlab.com/dart0n/dotfiles/-/raw/main/preview_1.png"><img src="https://gitlab.com/dart0n/dotfiles/-/raw/main/preview_1.png" width="700"></a>

# Dotfiles
## **My qtile and desktop config with a dwm-looking bar and pywal environment.**

>Using [archcraft](https://archcraft.io), so if something missing (rofi config etc.) or someting is wrong do it yourself, and dm me.

### Dependences:
* [qtile](http://www.qtile.org)
* [pywal](https://github.com/dylanaraps/pywal)
* [alacritty](https://alacritty.org)
* [nitrogen](http://projects.l3ib.org/nitrogen/)

### Getting started:
1. Clone this repo
```sh
git clone https://gitlab.com/d0space/dotfiles
```
2. Install the dependencies
```sh 
chmod +x ./dotfiles/install.sh
chmod +x ./dotfiles/qtile/install.sh
./dotfiles/install.sh
./dotfiles/qtile/install.sh
```
3. Copy the config files
```sh
mkdir /usr/share/backgrounds
cp dotfiles/etc /etc
cp dotfiles/qtile ~/.config/qtile
cp dotfiles/micro ~/.config/micro
cp dotfiles/usr/share/backgrounds/gruvbox /usr/share/backgrounds/gruvbox
```
4. Check the keybindings and default apps in config.py. And over other files. i.e. (groups.py, keys.py, widgets.py, etc2)
