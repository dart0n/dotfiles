# Global variables
u = "$(whoami)"
c = "/home/$u/.config"
d = "/home/$u/.dotfiles"
p = "/home/$u/Pictures"
f = "/home/$u/.dotfiles"
b = "$f/blackarch-installer/data"

# Update database
sudo pacman -Sy

# Install window manager
sudo pacman -S --noconfirm qtile python python3 python-pip python2-pip python-iwlib python-pywal
pip3 install psutil
wal --theme sexy-astromouse

# Install window manager extras
sudo pacman -S --noconfirm xclip lm_sensors alacritty micro imagemagick
yay -S xpenguins betterlockscreen

# Install another packages
sudo pacman -S --noconfirm htop gtop mlocate ripgrep neofetch screenfetch scrot feh nitrogen otf-fantasque-sans-mono noto-fonts noto-fonts-emoji noto-fonts-cjk ttf-jetbrains-mono ttf-joypixels ttf-font-awesome ttf-iosevka-nerd man-db zip unzip p7zip fish dhcpcd networkmanager xpdf wget curl
yay -S icecat-bin ttf-consolas-ligaturized ttf-mononoki
pip3 install fontawesome

# Install browser plugins
icecat https://noscript.net/releases/noscript-11.2.11.xpi
icecat https://www.eff.org/files/https-everywhere-latest.xpi
icecat https://github.com/gorhill/uBlock/releases/download/1.38.7b12/uBlock0_1.38.7b12.firefox.signed.xpi

# Install some base blackarch
cd "$f"
git clone https://github.com/BlackArch/blackarch-installer
curl -O https://blackarch.org/strap.sh
echo 46f035c31d758c077cce8f16cf9381e8def937bb strap.sh | sha1sum -c
chmod +x strap.sh
sudo ./strap.sh
sudo pacman --noconfirm -Syu

# Setting up LXDM
sudo pacman -S --noconfirm lxdm
mkdir -p "/usr/share/xsessions"
systemctl enable lxdm > /dev/null 2>&1

# Install base-modified arch
cd ~
sudo cp -r "$f/etc/X11" "/etc/"
sudo cp -r "$b/etc/lxdm" "/etc/lxdm/"
sudo cp -r "$b/usr/share/blackarch/lxdm" "/usr/share"
sudo cp -r "$b/usr/share/gtk-2.0" "/usr/share"

# Download and setup wallpapers
sudo mkdir /usr/share/backgrounds
git clone https://gitlab.com/exorcist365/wallpapers
sudo mv wallpapers /usr/share/backgrounds/exorcist
git clone https://gitlab.com/dwt1/wallpapers
sudo mv wallpapers /usr/share/backgrounds/dwt

# Setting up betterlockscreen
wget http://wallpaperswide.com/download/cyberman-wallpaper-1366x768.jpg -O "$p/cyber01.jpg"
betterlockscreen -u "$p/cyber01.jpg"

# Install kitty
sudo pacman --noconfirm -S kitty

# Setting up fish and oh-my-fish
sudo pacman -S --noconfirm pyenv fish
curl -k -L https://get.oh-my.fish | fish
fish -i -c "omf install z pure"

# Setting up configs
cd "$f"
cp -r fish/ $c/.
cp -r micro/ $c/.
cp -r qtile/ $c/.
cp -r alacritty/ $c/.
cp -r kitty/ $c/.
cp -r nitrogen/ $c/.
cp -r local/bin/ /home/$u/.local/.
